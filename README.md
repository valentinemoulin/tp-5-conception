# TP5-conception-logicielle
## Cloner le dépôt 
```
git clone https://gitlab.com/valentinemoulin/tp-5-conception.git
```

## Installer les dépendances 
Pour ce projet, vous aurez besoin d'installer toutes les dépendances qui sont dans requirements.txt. 
Dans votre terminal taper : 
```
pip install -r requirements.txt
```

## 
Pour faire tourner le code, taper dans votre terminal :
```
pip install main.py
```

## Pour faire tourner notre API :
Taper dans votre terminal :
````
uvicorn main:app --reload
````

Ouvrer ensuite sur un navigateur votre lien : http://127.0.0.1:8000.

#### Savoir si oui ou non votre produit est vegan. 
Munissez vous premièrement de l'identifiant du produit.

1 ) **Première possibilité** est d'entrer dans votre barre de recherche votre lien http et de rajouter 
" /id_produit?id_produit= " puis l'identifiant du produit dont vous souhaitez savoir s'il est ou non vegan

2 ) **Deuxième possibilité** : Ajouter de votre lien http /docs à la fin afin d'accéder à l'API. 
Une fois sur l'API, il vous suffit d'écrire l'identifiant de votre produit dans le rooter correspondant. 

Dans les deux cas : l'API vous reverra un booléan : 
    - true : le produit est vegan
    - false : le produit n'est pas vegan 

