from unicodedata import name
import requests
import fastapi
from fastapi import FastAPI



requete = requests.get("https://world.openfoodfacts.org/api/v0/product/3256540001305.json")
print(requete.status_code)
requete = requete.json()
#print(requete["product"]["ingredients"])

def isVegan(requestAsJson):
    ingredients = requestAsJson["product"]["ingredients"]
    for ingredient in ingredients : 
         if 'vegan' in ingredient:
            if ingredient['vegan'] == 'no' or ingredient['vegan'] == 'maybe':
                return False 
    return True

print(isVegan(requete))


app = FastAPI(
    title="VeganOrNot",
    description="Prédit si un aliment est vegan ou non",
    version="1.0.0")


@app.get("/id_produit")
async def vegan_or_not(id_produit):
    try : 
        requete = requests.get("https://world.openfoodfacts.org/api/v0/product/[{}.json".format(id_produit))
        output = isVegan(requete.json())
    except : 
        output = "L'identifiant du produit n'existe pas dans l'API OpenFoodFact"
    return output 



if __name__ == "__main__":
    import uvicorn
    uvicorn.run("main:app", host="127.0.0.1", port=8000)



