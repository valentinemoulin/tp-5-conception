fastapi==0.75.0
pytest==7.1.1
requests==2.26.0
uvicorn==0.15.0
